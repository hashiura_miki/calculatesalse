package jp.alhink.hashiura_miki.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Readtest {
	public static void main(String[] args) {
		try {
			class read {
				String id;
				String shop;
				void printShop() {
					System.out.println(this.id + "," + this.shop);
				}
			}


			File readList = new File(args[0],"branch.lst");   //支店定義ファイル
			FileReader rL = new FileReader(readList);
			BufferedReader bL = new BufferedReader(rL);

			String list = bL.readLine();
			int i=0;
			while(list != null) {
				read list1 = new read();
				if(i%2==0)
				list1.id = list;
				else
				list1.shop = list;

				list1.printShop();
				list = bL.readLine();
				i++;
				System.out.println("------");
			}

			bL.close();

		}
		catch (FileNotFoundException e) {
			System.out.println(e);
		}
		catch (IOException e) {
			System.out.println(e);
		}
	}
}
