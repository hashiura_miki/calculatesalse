package jp.alhinc.hashiura_miki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		BufferedReader br = null;
		BufferedReader sr = null;

		Map<String, String> branchMap = new HashMap<>();       //コード、支店名のマップ
		Map<String,Long> salesMap = new HashMap<>();        //コード、合計金額のマップ


		//支店定義lstファイルの読み込み

		try {
			if (args.length != 1){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			File file = new File(args[0],"branch.lst");

			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String str = null;

			while((str = br.readLine()) != null){
				String[] branchInfo = str.split(",");
				String code = branchInfo[0];

				if (code.matches("^[0-9]{3}+$") == false) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				if (branchInfo.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				String branch = branchInfo[1];

				//コード、支店名のマップ
				branchMap.put(code, branch);
				//コード、合計金額のマップ
				salesMap.put(code, 0L);
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if (br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


		List <File> rcdAll = new ArrayList<File>();

		//Fileクラスのオブジェクトを生成する
		File dir = new File(args[0]);

		//listFilesメソッドを使用して一覧を取得する
		File[] list = dir.listFiles();

		for (int i = 0 ; i < list.length; i ++) {

			//8桁のrcdファイルの確認
			if(list[i].getName().matches("[0-9]{8}.rcd") && list[i].isFile()) {
				rcdAll.add(list[i]);
			}

			//売上rcdファイルの読み込み

			for (int s = 0; s < rcdAll.size() -1 ;  s++) {

				String serialNumber1 = rcdAll.get(s).getName().substring(0, 8);
				String serialNumber2 = rcdAll.get(s+1).getName().substring(0, 8);
				int serialNum1 = Integer.parseInt(serialNumber1);
				int serialNum2 = Integer.parseInt(serialNumber2);

				if(serialNum2 - serialNum1 != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			try {
				for (int j = i; j < rcdAll.size();  j++) {

					List <String> saleData = new ArrayList<String>();
					//args[0]にあるrcdファイルのj番目を開く
					File rcdFile = new File(args[0],rcdAll.get(j).getName());
					FileReader salefr = new FileReader(rcdFile);
					sr = new BufferedReader(salefr);
					String saleFile;

					while((saleFile = sr.readLine()) != null){
						saleData.add(saleFile);
					}

					if (saleData.size() != 2) {
						System.out.println(rcdFile.getName() + "のフォーマットが不正です");
						return;
					}

					if (saleData.get(1).matches("^[0-9]+$") == false) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}

					if(branchMap.containsKey(saleData.get(0)) == false){
						System.out.println(rcdFile.getName() + "の支店コードが不正です");
						return;
					}

					Long amount = salesMap.get(saleData.get(0));
					amount += Long.parseLong(saleData.get(1));
					salesMap.put(saleData.get(0), amount);

					if(String.valueOf(amount).length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if (sr != null) {
					try {
						sr.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		if(!(fileout(args[0] , "branch.out" , salesMap , branchMap))){
			return;
		}
	}


	//outファイルへの出力メソッド
	public static boolean fileout(String dir , String outFile, Map<String,Long> codeSales , Map<String,String> name) {
		BufferedWriter wr = null;
		try {
			File writeFile = new File(dir + "\\" + outFile);
			FileWriter fw = new FileWriter(writeFile);

			wr = new BufferedWriter(fw);

			//環境依存しない改行コード
			String next = System.getProperty("line.separator");
			for (Map.Entry<String, Long> entry : codeSales.entrySet()) {
				String line = entry.getKey() + "," + name.get(entry.getKey())+  "," + entry.getValue() + next;
				fw.write(line);
			}
		}catch (IOException ex) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if (wr != null) {
				try {
					wr.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}